package ro.ubb.springjpa.ui;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.ubb.springjpa.model.Book;
import ro.ubb.springjpa.model.Client;
import ro.ubb.springjpa.model.Exceptions.RepositoryException;
import ro.ubb.springjpa.model.Exceptions.ValidatorException;
import ro.ubb.springjpa.model.Purchase;
import ro.ubb.springjpa.repository.Sort;
import ro.ubb.springjpa.service.ServiceServer.BookServiceServer;
import ro.ubb.springjpa.service.ServiceServer.ClientServiceServer;
import ro.ubb.springjpa.service.ServiceServer.PurchaseServiceServer;
import ro.ubb.springjpa.service.ServiceServer.Reports;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 * Created by radu.
 */
@Component
public class Console {
    @Autowired
    private ClientServiceServer clientService;
    @Autowired
    private PurchaseServiceServer purchaseService;
    @Autowired
    private BookServiceServer bookService;
    @Autowired
    private Reports reports;

    public void runConsole() throws IOException {
        boolean ok=true;

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        while(ok) {
            System.out.println("Please choose one of the above:" +
                    "\n1.Add book" +
                    "\n2.Update book" +
                    "\n3.Delete book" +
                    "\n4.Filter books by title" +
                    "\n5.Filter books by price" +
                    "\n6.Print all books" +
                    "\n7.Add client" +
                    "\n8.Delete client" +
                    "\n9.Update client" +
                    "\n10.Filter clients by name" +
                    "\n11.Filter clients by number of books" +
                    "\n12.Print all clients" +
                    "\n13.Buy a book" +
                    "\n14.Return a book" +
                    "\n15.Modify a purchase date" +
                    "\n16.Filter purchases by date : 'Before'" +
                    "\n17.Print all purchases" +
                    "\n18.Sort the clients by the money spent on books" +
                    "\n19.Sort the clients by name" +
                    "\n20.Sort the Books by price ascending and descending by name" +
                    "\n21.Sort purchases by clientID ascending and ascending by bookID" +


                    "\n0.exit\n");

            int a = Integer.parseInt(bufferRead.readLine());
            try {
                switch (a) {
                    case 0:
                        ok = false;
                        break;
                    case 1:
                        addBook();
                        break;
                    case 2:
                        updateBook();
                        break;
                    case 3:
                        deleteBook();
                        break;
                    case 4:
                        filterBooksByTitle();
                        break;
                    case 5:
                        filterBooksByPrice();
                        break;
                    case 6:
                        printAllBooks();
                        break;
                    case 7:
                        addClient();
                        break;
                    case 8:
                        deleteClient();
                        break;
                    case 9:
                        updateClient();
                        break;
                    case 10:
                        filterClientsByName();
                        break;
                    case 11:
                        filterClientsByNoBooks();
                        break;
                    case 12:
                        printAllClients();
                        break;
                    case 13:
                        buy();
                        break;
                    case 14:
                        reTurn();
                        break;
                    case 15:
                        changeDate();
                        break;
                    case 16:
                        getAllPurchasesBefore();
                        break;
                    case 17:
                        printAllPurchases();
                        break;
                    case 18:
                        sortClientsBySpentMoney();
                        break;
                    case 19:
                        sortClientsByName();
                        break;
                    case 20:
                        sortBooks();
                        break;
                    case 21:
                        sortPurchases();
                        break;

                }
            }catch (IOException | ValidatorException| RepositoryException | ParseException ex){
                System.err.println(ex.toString());
            }

        }

    }

    private void printAllPurchases() {
        purchaseService.getAllPurchases().forEach(System.out::println);
    }

    private void getAllPurchasesBefore() throws IOException, ParseException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("give the date for filtering\n");
        String lineInput;
        lineInput = bufferRead.readLine();
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(lineInput);
        reports.filterPurchasesByDateBefore(date).forEach(System.out::println);
    }

    private void changeDate() throws IOException, ParseException, ValidatorException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("give the client id\n");
        Long clientId = Long.valueOf(bufferRead.readLine());// ...
        System.out.println("give the book id\n");
        Long bookId = Long.valueOf(bufferRead.readLine());//
        System.out.println("give the new Date\n");
        String lineInput;
        lineInput = bufferRead.readLine();
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(lineInput);
        purchaseService.changeDate(new Purchase(clientId,bookId,date));
    }

    private void reTurn() throws IOException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("give the client id\n");
        Long clientId = Long.valueOf(bufferRead.readLine());// ...
        System.out.println("give the book id\n");
        Long bookId = Long.valueOf(bufferRead.readLine());//
        purchaseService.reTurn(clientId,bookId);

    }

    private void buy() throws IOException, ParseException, ValidatorException {

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("give the client id\n");
        Long clientId = Long.valueOf(bufferRead.readLine());// ...
        System.out.println("give the book id\n");
        Long bookId = Long.valueOf(bufferRead.readLine());//
        System.out.println("give the date of the purchase\n");
        String lineInput;
        lineInput = bufferRead.readLine();
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(lineInput);
        purchaseService.buy(new Purchase(clientId,bookId,date));

    }

    private Long readId(){
        Scanner inn = new Scanner(System.in);
        System.out.println("Enter the id of the entity:\n");
        return inn.nextLong();
    }

    private void filterBooksByTitle() {
        Scanner inn = new Scanner(System.in);
        System.out.println("Enter the filter parameter\n");
        String str = inn.nextLine();
        System.out.println("Filtered Books (title containing '"+str+"'):\n");
        reports.filterBooksByTitles(str).forEach(System.out::println);
    }

    private void filterBooksByPrice() {
        Scanner inn = new Scanner(System.in);
        System.out.println("Enter the price you want to filter by:\n");
        int price = inn.nextInt();
        System.out.println("Filtered Books (price greater than '"+ price +"'):\n");
        reports.removeBookIfPriceLessThenS(price).forEach(System.out::println);
    }

    private void printAllBooks() {
        bookService.getAllBook().forEach(System.out::println);
    }
    private void printAllClients() {
        clientService.getAllClient().forEach(System.out::println);
    }

    private void addBook() throws IOException, ValidatorException {

        Book book = readBook();
//        Long id = readId();
//        book.setId(id);
        bookService.addBook(book);
    }

    private void deleteBook() {
        Long id = readId();
        bookService.deleteBook(id);
    }
    private void updateBook() throws IOException, ValidatorException {

        Book book = readBook();
        Long id = readId();
        book.setId(id);
        bookService.updateBook(book);
    }


    private Book readBook() throws IOException {
        System.out.println("Read Book {id,title, author, price, quantity}\n");
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Give the Title of the book\n");
        String title = bufferRead.readLine();
        System.out.println("Give the author of the book\n");
        String author = bufferRead.readLine();
        System.out.println("Give the price of the book\n");
        int price = Integer.parseInt(bufferRead.readLine());// ...

        return new Book(title,author,price);

    }

    private void addClient() throws IOException, ValidatorException {
        Client client = readClient();
//        Long id = readId();
//        client.setId(id);
        clientService.addClient(client);
    }

    private void deleteClient() {
        Long id = readId();
        clientService.deleteClient(id);
    }

    private void updateClient() throws IOException, ValidatorException {
        Client client = readClient();
        Long id = readId();
        client.setId(id);
        clientService.updateClient(client);
    }
    private Client readClient() throws IOException {
        System.out.println("Read Client {name}\n");
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Give the name of the client\n");
        String name = bufferRead.readLine();
        return new Client(name);
    }

    private void filterClientsByName(){
        Scanner inn = new Scanner(System.in);
        System.out.println("Enter the filter parameter\n");
        String str = inn.nextLine();
        System.out.println("Filtered Clients (name containing '"+str+"'):\n");
        reports.filterClientsByName(str).forEach(System.out::println);
    }

    private void filterClientsByNoBooks(){
        Scanner inn = new Scanner(System.in);
        System.out.println("Enter the NO books a client must have:\n");
        int booksNO = inn.nextInt();
        System.out.println("Filtered Clients (NO books greater than '"+ booksNO +"'):\n");
        reports.filterClientsWithMoreThanXBooks(booksNO).forEach(System.out::println);
    }

    private void sortClientsBySpentMoney(){
        reports.sortClientsByMoneySpent().forEach(System.out::println);
    }


    private void sortClientsByName(){
        Scanner inn = new Scanner(System.in);
        System.out.println("Enter 1 for ascending sorting or 2 for descending sorting\n");
        int x = inn.nextInt();
        Sort sort ;
        if(x == 1){
            sort = new Sort("name");
        }
        else sort = new Sort(Sort.Dir.descending,"name");
        System.out.println();
        clientService.getSortedClients(sort).forEach(System.out::println);

    }

    private void sortBooks(){
        Sort sort = new Sort("price").add(new Sort(Sort.Dir.descending,"title"));
       bookService.getSortedBooks(sort).forEach(System.out::println);
    }

    private void sortPurchases(){
        Sort sort = new Sort("idClient","idBook");
        purchaseService.getSortedPurchases(sort).forEach(System.out::println);

    }
}
