package ro.ubb.springjpa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;

@Entity
public class Purchase extends BaseEntity<Long> {
    private Long idClient;
    private Long idBook;
    private Date date;

    static private AtomicLong id = new AtomicLong(1);
    public Purchase(Long idClient, Long idBook, Date date) {
        this.idClient = idClient;
        this.idBook = idBook;
        this.date = date;
        this.setId(Purchase.id.getAndIncrement());
    }

    public Purchase() {
    }

    public static AtomicLong getIdAtomic() {
        return id;
    }

    public Long getIdClient() {
        return idClient;
    }

    public void setIdClient(Long idClient) {
        this.idClient = idClient;
    }

    public Long getIdBook() {
        return idBook;
    }

    public void setIdBook(Long idBook) {
        this.idBook = idBook;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Purchase{" +
                "idClient=" + idClient +
                ", idBook=" + idBook +
                ", date='" + date + '\'' +
                '}';
    }
}
