package ro.ubb.springjpa.model.Validators;

import org.springframework.stereotype.Component;
import ro.ubb.springjpa.model.Client;
import ro.ubb.springjpa.model.Exceptions.ValidatorException;
@Component
public class ClientValidator  {

    static public void validate(Client entity) throws ValidatorException {
        if (entity==null) throw new ValidatorException("There is no client. Sorry.\n");
        if (entity.getName().equals("")) throw new ValidatorException("There is no name\n");
    }
}
