package ro.ubb.springjpa.model.Validators;

import org.springframework.stereotype.Component;
import ro.ubb.springjpa.model.Exceptions.ValidatorException;
import ro.ubb.springjpa.model.Purchase;
@Component
public class PurchaseValidator  {

    static public void validate(Purchase entity) throws ValidatorException {
        if (entity==null) throw new ValidatorException("There is no purchase. Sorry.\n");
        if (entity.getDate() == null) throw new ValidatorException("There is no date\n");
        if (entity.getIdBook()<=0) throw new ValidatorException("OOPS! BookId is invalid!!\n");
        if (entity.getIdClient()<=0) throw new ValidatorException("OOPS! ClientId is invalid!!\n");
    }}

