package ro.ubb.springjpa.model.Validators;
import org.springframework.stereotype.Component;
import ro.ubb.springjpa.model.Book;
import ro.ubb.springjpa.model.Exceptions.ValidatorException;
@Component
public class BookValidator{

    public static void validate(Book entity) throws ValidatorException {
        if (entity==null) throw new ValidatorException("There is no book. Sorry.\n");
        if (entity.getAuthor().length()<3) throw new ValidatorException("String given is not a valid author.\n");
        if (entity.getTitle().length()<3) throw new ValidatorException("String given is not a valid Title!\n");
        if (entity.getPrice()<=0) throw new ValidatorException("The Price of the book should be greater than 0\n");

    }
}
