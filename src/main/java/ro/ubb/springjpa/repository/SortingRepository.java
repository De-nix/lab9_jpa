package ro.ubb.springjpa.repository;

import org.springframework.data.repository.NoRepositoryBean;
import ro.ubb.springjpa.model.BaseEntity;

import java.io.Serializable;
import java.util.List;

@NoRepositoryBean
public interface SortingRepository< T extends BaseEntity<ID>,ID extends Serializable> extends Repository< T,ID>
{


   default List<T> findAll(Sort sort){
       List<T> list = this.findAll();
       list.sort(sort);
       return list;
   };

    //TODO: insert sorting-related code here
}