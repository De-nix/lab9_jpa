package ro.ubb.springjpa.repository.DBinterfaces;

import ro.ubb.springjpa.model.Purchase;
import ro.ubb.springjpa.repository.SortingRepository;

public interface PurchaseRepo extends SortingRepository<Purchase,Long> {
}
