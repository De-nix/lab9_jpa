package ro.ubb.springjpa.repository.DBinterfaces;

import ro.ubb.springjpa.model.Client;
import ro.ubb.springjpa.repository.SortingRepository;

public interface ClientRepo extends SortingRepository<Client,Long> {
}
