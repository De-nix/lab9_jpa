package ro.ubb.springjpa.repository.DBinterfaces;


import ro.ubb.springjpa.model.Book;
import ro.ubb.springjpa.repository.SortingRepository;

public interface BookRepo extends SortingRepository<Book,Long> {
}
