package ro.ubb.springjpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ro.ubb.springjpa.model.BaseEntity;

import java.io.Serializable;

/**
 * Interface for generic CRUD operations on a repository for a specific type.
 */
@NoRepositoryBean
public interface Repository< T extends BaseEntity<ID>,ID extends Serializable> extends JpaRepository<T,ID> {

}
