package ro.ubb.springjpa.service.ServiceServer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ubb.springjpa.model.Client;
import ro.ubb.springjpa.model.Exceptions.ValidatorException;
import ro.ubb.springjpa.model.Purchase;
import ro.ubb.springjpa.repository.Sort;
import ro.ubb.springjpa.service.Service.ClientServiceImpl;
import ro.ubb.springjpa.service.Service.PurchaseServiceImpl;

import java.io.IOException;
import java.util.List;
import java.util.Set;

@Service
public class ClientServiceServer  {
    @Autowired
    ClientServiceImpl clientServiceImpl;
    @Autowired
    PurchaseServiceImpl purchaseServiceImpl;


    /**
     * @param client - the client that will be added
     * @return
     */
    public synchronized Client addClient(Client client) throws ValidatorException {

        return clientServiceImpl.addClient(client);
    }

    /**
     * @param id - the client that will be deleted
     *
     */
    public synchronized void deleteClient(Long id)  {

        clientServiceImpl.getClient(id);
        purchaseServiceImpl.getAllPurchases().stream().filter(x -> x.getIdClient().equals(id)).map(Purchase::getIdBook).forEach(x -> {
            purchaseServiceImpl.deletePurchase( id,x);
        });
        clientServiceImpl.deleteClient(id);
    }

    /**
     * @param client - the updated client with the same id as the old one
     * @return

     */
    public synchronized Client updateClient(Client client) throws ValidatorException {

        return clientServiceImpl.updateClient(client);

    }

    /**
     * @param id - the id of a client
     * @return - the client with the id = 'id'
     */
    public synchronized Client getClient(Long id) {


            return clientServiceImpl.getClient(id);


    }

    /**
     * @return - a set of all the clients
     */

    public synchronized Set<Client> getAllClient() {
        return clientServiceImpl.getAllClients();
    }
    public synchronized List<Client> getSortedClients(Sort sort) {
        return clientServiceImpl.getClientsSortedByName(sort);
    }

}
