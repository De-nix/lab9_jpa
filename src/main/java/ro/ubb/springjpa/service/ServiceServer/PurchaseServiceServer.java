package ro.ubb.springjpa.service.ServiceServer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import ro.ubb.springjpa.model.Book;
import ro.ubb.springjpa.model.Client;
import ro.ubb.springjpa.model.Exceptions.RepositoryException;
import ro.ubb.springjpa.model.Exceptions.ValidatorException;
import ro.ubb.springjpa.model.Purchase;
import ro.ubb.springjpa.repository.Sort;
import ro.ubb.springjpa.service.Service.BookServiceImpl;
import ro.ubb.springjpa.service.Service.ClientServiceImpl;
import ro.ubb.springjpa.service.Service.PurchaseServiceImpl;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Set;


@Service
public class PurchaseServiceServer  {
    @Autowired
    BookServiceImpl bookServiceImpl;
    @Autowired
    ClientServiceImpl clientServiceImpl;
    @Autowired
    PurchaseServiceImpl purchaseServiceImpl;

    public synchronized Set<Purchase> getAllPurchases() {

     return  purchaseServiceImpl.getAllPurchases();

    }


    public synchronized List<Purchase> getSortedPurchases(Sort sort) {
        return purchaseServiceImpl.getPurchasesSorted(sort);
    }

    public synchronized Purchase buy(Purchase purchase) throws RepositoryException, ValidatorException {

        Client x = clientServiceImpl.getClient(purchase.getIdClient());
        if(x == null) throw new RepositoryException("The client id is invalid\n");
        Book y = bookServiceImpl.getBook(purchase.getIdBook());

        if(y == null) throw new RepositoryException("The book id is invalid\n");
        if (purchaseServiceImpl.findPurchase(purchase.getIdClient(), purchase.getIdBook()) == -1) {
             return purchaseServiceImpl.addPurchase(purchase);
        } else throw new RepositoryException("The is already a purchase between these ids\n");
    }

    public synchronized void reTurn(Long clientId, Long bookId) {

        Client x = clientServiceImpl.getClient(clientId);
        if(x == null) throw new RepositoryException("The client id is invalid\n");
        Book y = bookServiceImpl.getBook(bookId);
        if(y == null) throw new RepositoryException("The book id is invalid\n");
        purchaseServiceImpl.deletePurchase(clientId, bookId);
    }

    public synchronized Purchase changeDate(Purchase purchase) throws ValidatorException {

        return purchaseServiceImpl.updatePurchase(purchase);
    }


    public synchronized Purchase getPurchase(Long clientId, Long bookId) {
        Long id = purchaseServiceImpl.findPurchase(clientId, bookId);
        if (id == -1L)return null;
        return purchaseServiceImpl.getPurchase(id);
    }

}

