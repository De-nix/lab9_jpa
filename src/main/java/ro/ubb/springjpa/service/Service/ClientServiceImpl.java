package ro.ubb.springjpa.service.Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ubb.springjpa.model.Book;
import ro.ubb.springjpa.model.Client;
import ro.ubb.springjpa.model.Exceptions.ValidatorException;
import ro.ubb.springjpa.model.Validators.ClientValidator;
import ro.ubb.springjpa.repository.DBinterfaces.ClientRepo;
import ro.ubb.springjpa.repository.Sort;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ClientServiceImpl {
    @Autowired
    private ClientRepo repository;


    public static final Logger log = LoggerFactory.getLogger(ClientServiceImpl.class);

    public synchronized Client addClient(Client client) throws ValidatorException {

        log.trace("addClient - method entered: client={}", client);
        ClientValidator.validate(client);
        Client result =  repository.save(client);
        log.trace("addClient - method finished");
        return result;
    }

    public synchronized void deleteClient(Long id) {
        log.trace("deleteClient - method entered: id{}",id);
        repository.deleteById(id);
        log.trace("deleteClient - method finished");
    }

    public synchronized List<Client> getClientsSortedByName(Sort sort){
        log.trace("getClientsSortedByName: method entered");
        Iterable<Client> clients = repository.findAll(sort);
        List<Client> l = new ArrayList<>();
        clients.forEach(l::add);
        log.trace("getClientsSortedByName: method finished");
        return l;
    }

    public synchronized Client getClient(Long id) {

        log.trace("getClient - method entered: id{}",id);
        Client result = repository.findById(id).orElse(null);
        log.trace("getClient - method finished");
        return result;
    }
    @Transactional
    public synchronized Client updateClient(Client client) throws ValidatorException {
        ClientValidator.validate(client);
        log.trace("updateClient - method entered: client={}", client);
        Optional<Client> optionalClient = repository.findById(client.getId());
        optionalClient.ifPresent(s -> {
            s.setName(client.getName());
            log.debug("updateClient - updated: s={}", s);
        });

        log.trace("updateClient - method finished");
        return optionalClient.orElse(null);
    }

    public synchronized Set<Client> getAllClients() {
        Iterable<Client> clients = repository.findAll();
        return StreamSupport.stream(clients.spliterator(), false).collect(Collectors.toSet());
    }


}
