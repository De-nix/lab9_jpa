package ro.ubb.springjpa.service.Service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ubb.springjpa.model.Book;
import ro.ubb.springjpa.model.Exceptions.ValidatorException;
import ro.ubb.springjpa.model.Validators.BookValidator;
import ro.ubb.springjpa.repository.DBinterfaces.BookRepo;
import ro.ubb.springjpa.repository.Sort;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class BookServiceImpl {
    @Autowired
    private BookRepo repository;


    public static final Logger log = LoggerFactory.getLogger(BookServiceImpl.class);

    public synchronized Book addBook(Book book) throws ValidatorException {

        log.trace("addBook - method entered: book={}", book);
        BookValidator.validate(book);
        Book result = repository.save(book);
        log.trace("addBook - method finished");
        return result;
    }

    public synchronized void deleteBook(Long id) {
        log.trace("deleteBook - method entered: id={}", id);
        repository.deleteById(id);
        log.trace("deleteBook - method finished");
    }

    @Transactional
    public synchronized Book updateBook(Book book) throws ValidatorException {
        log.trace("updateBook - method entered: book={}", book);
        BookValidator.validate(book);
        Optional<Book> optionalBook = repository.findById(book.getId());
        optionalBook.ifPresent(s -> {
            s.setTitle(book.getTitle());
            s.setAuthor(book.getAuthor());
            s.setPrice(book.getPrice());
            log.debug("updateBook - updated: s={}", s);
        });
        log.trace("updateBook - method finished");
        return optionalBook.orElse(null);

    }

    public synchronized Book getBook(Long id) {
        log.trace("getBook - method entered: id={}", id);
        Book result = repository.findById(id).orElse(null);
        log.trace("getBook - method finished");
        return result;
    }

    public synchronized List<Book> getBooksSorted(Sort sort) {
        log.trace("getBooksSorted - method entered: ");
        List<Book> result = repository.findAll(sort);
        log.trace("getBooksSorted - method finished");
        return result;
    }

    public synchronized Set<Book> getAllBooks() {

        log.trace("getAllBooks - method entered: ");
        Iterable<Book> Books = repository.findAll();
        Set<Book> result = StreamSupport.stream(Books.spliterator(), false).collect(Collectors.toSet());
        log.trace("getAllBooks - method finished: ");
        return result;
    }


}
